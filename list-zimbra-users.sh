#!/bin/bash

#before running  this script, check that all users have a displayName, including system accounts
   if  [ -f "/root/listado_de_cuentas.xml" ]; then
    rm -f /root/listado_de_cuentas.xml
   else
    echo "No existe listado_de_cuentas.xml"
   fi

   echo "We are going to export a .xml file with a list of all zimbra users"
   sleep 3s
   su - zimbra -c "zmsoap -z -v SearchDirectoryRequest @offset="0" @limit="500" @sortBy="name" @sortAscending="1" @applyCos="false" @applyConfig="false" @attrs="displayName,uid,zimbraAccountStatus,zimbraMailStatus" @types="accounts" @maxResults="5000" query=""" >mail.xml
   cat mail.xml | grep "name=\|displayName\|</account>\|zimbraAccountStatus">>mail_temp.xml
   echo "Done!"

  texto=/root/mail_temp.xml
  can_read="1"
  echo "<zimbraMailAccounts>">listado_de_cuentas.xml
  while read line
  do
   case $line in
   *"isExternal"*)
     if grep -v virus-quarantine<<<$line;then
       echo "<mailAccount>">>listado_de_cuentas.xml
       echo  $line | awk '{print "<mail>"$3"</mail>"}' | sed 's/name="//g' | sed 's/"//g'>> listado_de_cuentas.xml
       can_read="1"
     else
       can_read="0"
     fi
   ;;
   *"zimbraAccountStatus"*)
     if [ "$can_read" -eq '1' ];then
       if grep -q active <<< $line; then
         echo "<zimbraAccountStatus>active</zimbraAccountStatus>">>listado_de_cuentas.xml
         elif grep -q closed <<< $line; then
         echo "<zimbraAccountStatus>closed</zimbraAccountStatus>">>listado_de_cuentas.xml
       fi
     else
       can_read="1"
     fi
   ;;
   *"displayName"*)
     newline=$(echo $line | sed 's/<a n="displayName">//g' | sed 's/[/]//g' | sed 's/<a>//g')
     echo "<displayName>"$newline"</displayName>">>listado_de_cuentas.xml
     echo "</mailAccount>">>listado_de_cuentas.xml
   ;;
   esac
  done<$texto
  echo "</zimbraMailAccounts>">>listado_de_cuentas.xml

  rm -f mail.xml
  rm -f mail_temp.xml
  exit
